package cn.tengfei;

import cn.tengfei.ocr.OcrApi;
import com.baidu.aip.ocr.AipOcr;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TengfeiFileSystemApplicationTests {

    @Autowired
    OcrApi ocrApi;

    @Test
    public void contextLoads() {
    }

    @Test
    public void testBaiduApi() throws JSONException {
        AipOcr apiOcr = ocrApi.getApiOcr();
        String path = "/Users/jesse/Documents/images/11.jpg";
        JSONObject res = apiOcr.basicGeneral(path, new HashMap<String, String>());
        System.out.println(res.toString(2));

    }

}
