package cn.tengfei.ocr;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class OcrSingleton {

    @Value("${baidu.ocr.appId}")
    public static final String APP_ID = "你的 App ID";
    @Value("${baidu.ocr.appKey}")
    public static final String API_KEY = "你的 Api Key";
    @Value("${baidu.ocr.secretKey}")
    public static final String SECRET_KEY = "你的 Secret Key";

    private OcrSingleton() {
    }

    public static OcrSingleton getInstance() {
        return EnumSingleton.INSTANCE.getInstance();
    }

    private enum EnumSingleton {
        INSTANCE;
        private OcrSingleton ocrSingleton;

        EnumSingleton() {
            ocrSingleton = new OcrSingleton();
        }

        public OcrSingleton getInstance() {
            return ocrSingleton;
        }
    }


}
