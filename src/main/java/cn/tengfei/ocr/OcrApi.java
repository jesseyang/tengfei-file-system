package cn.tengfei.ocr;


import com.baidu.aip.ocr.AipOcr;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@Component
public class OcrApi {
    @Value("${baidu.ocr.appId}")
    private String APP_ID;
    @Value("${baidu.ocr.appKey}")
    private String API_KEY = "你的 Api Key";
    @Value("${baidu.ocr.secretKey}")
    private String SECRET_KEY = "你的 Secret Key";

    AipOcr apiOcr;

    public AipOcr getApiOcr() {
        if (apiOcr == null) {
            apiOcr = new AipOcr(APP_ID, API_KEY, SECRET_KEY);
        }
        return apiOcr;
    }

    public List<String> ocrByFilePath(String imagePath) {
        getApiOcr();
        JSONObject res = apiOcr.basicGeneral(imagePath, new HashMap<String, String>());

        return dealOcrResultMap(res);
    }

    private List<String> dealOcrResultMap(JSONObject ocrResultJson) {
        JSONArray wordsArray = ocrResultJson.getJSONArray("words_result");
        Iterator<Object> iterator = wordsArray.iterator();
        List<String> wordList = new ArrayList<>(128);
        while (iterator.hasNext()) {
            JSONObject wordJson = (JSONObject) iterator.next();
            wordList.add(wordJson.getString("words"));

        }
        return wordList;

    }
    public List<String> ocrByImgUrl(String url){
        getApiOcr();
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("language_type", "CHN_ENG");
        options.put("detect_direction", "true");
        options.put("detect_language", "true");
        options.put("probability", "true");
        JSONObject res = apiOcr.basicGeneralUrl(url,options);
        System.out.println(res.toString(2));
        System.out.println(url);
        return dealOcrResultMap(res);

    }

}
