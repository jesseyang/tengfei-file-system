package cn.tengfei.controller;

import cn.tengfei.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: JesseYang
 * @Date: 2018/11/17 23:17
 * @Description:
 */
@Controller
@RequestMapping("file")
public class FileController {
    @Autowired
    private FileService fileService;

    @RequestMapping("upload")
    @ResponseBody
    public Object fileUpload(@RequestParam MultipartFile file) {
        String fileName = fileService.uploadFile(file);
        Map<String, String> map = new HashMap<>();
        map.put("fileName", fileName);
        return map;
    }

    @RequestMapping("fileList")
    @ResponseBody
    public Object fileList() {

        return fileService.getServerFileNameList();
    }
}
