package cn.tengfei.controller;

import cn.tengfei.ocr.OcrApi;
import cn.tengfei.service.FileOcrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("ocr")
public class OcrController {

    @Autowired
    OcrApi ocrApi;

    @Autowired
    FileOcrService fileOcrService;

    @RequestMapping("upload")
    public Object ocrByFile(@RequestParam("file") MultipartFile file) {
        return fileOcrService.ocrByMultipartFile(file);
    }

    @RequestMapping("url")
    public Object url(@RequestParam("url") String url) {
        return ocrApi.ocrByImgUrl(url);
    }
}
