package cn.tengfei.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: JesseYang
 * @Date: 2018/11/17 23:02
 * @Description:
 */
@Controller
public class IndexController {

    @Value("${serverUrl}")
    private String serverUrl;

    private Map<String, Object> params = new HashMap<>();

    @RequestMapping("/")
    public String index(){
        return "file";
    }

    @RequestMapping("/back")
    public ModelAndView back() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("back");
        params.put("serverUrl", serverUrl);
        modelAndView.addObject("params", params);
        return modelAndView;
    }

    @RequestMapping("/ocr")
    public ModelAndView ocr() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("ocr");
        return modelAndView;
    }


}
