package cn.tengfei.controller;

import cn.tengfei.http.HttpRequest;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @Auther: JesseYang
 * @Date: 2018/11/17 23:17
 * @Description:
 */

@Controller
@RequestMapping("wx")
public class WxController {

    @Value("${wx.appId}")
    private String appId;
    @Value("${wx.appSecret}")
    private String appSecret;

    private static Map<String,String> accessTokenMap = new HashMap<>();

    @GetMapping("getAccessToken")
    @ResponseBody
    public Object getAccessToken(){

        return accessTokenMap.get("accessToken");
    }

    @GetMapping("wxAccessToken")
    @ResponseBody
    public Object wxAccessToken(@RequestParam(value="needRefresh",required = false) String needRefresh){
        System.out.println(appId+"|"+appSecret);
        if("1".equals(needRefresh) || accessTokenMap.get("accessToken") == null){
            String result =  HttpRequest.sendGet("https://api.weixin.qq.com/cgi-bin/token","grant_type=client_credential&appid="+appId+"&secret="+appSecret);
            accessTokenMap.put("accessToken",result);
            System.out.println("request access token");
        }
        return accessTokenMap.get("accessToken");
    }

    @GetMapping("wxGetToken")
    @ResponseBody
    public Object wxAccessToken(@RequestParam(value="appId") String appId,@RequestParam(value="appSecret") String appSecret){

        String result =  HttpRequest.sendGet("https://api.weixin.qq.com/cgi-bin/token","grant_type=client_credential&appid="+appId+"&secret="+appSecret);
        accessTokenMap.put("accessToken",result);
        System.out.println("request access token");

        return accessTokenMap.get("accessToken");
    }

    @PostMapping("request")
    @ResponseBody
    public Object request(@RequestBody Map params){
        System.out.println(params.toString());
        Set set = params.keySet();
        Iterator iterator = set.iterator();
        JSONObject jsonObject = new JSONObject();
        while (iterator.hasNext()){
            Object key =  iterator.next();
            if(key!=null){
                jsonObject.put(key.toString(),params.get(key));
            }
        }
        String url  = params.get("url").toString();
        jsonObject.remove("url");
        return HttpRequest.doPost(url,jsonObject).toString();
    }

}
