package cn.tengfei.file;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Auther: JesseYang
 * @Date: 2018/11/18 10:18
 * @Description:
 */
@Component
public class ServerFileAction implements FileAction {

    private final static Integer OVER_DATE_NUM = 50;

    @Override
    public String uploadFile(MultipartFile multipartFile, String dirPath) throws IOException {
        String fileName = multipartFile.getOriginalFilename();
        String fileSuffix = fileName.substring(fileName.lastIndexOf("."), fileName.length());
        String localFileName = System.currentTimeMillis() + fileSuffix;
        String filePath = dirPath + File.separator + localFileName;
        File localFile = new File(filePath);
        File imagePath = new File(dirPath);
        if (!imagePath.exists()) {
            imagePath.mkdirs();
        }
        multipartFile.transferTo(localFile);
        return localFileName;
    }

    @Override
    public List<String> getFileNamesFromPath(String filePath) {
        List<String> fileNames = new ArrayList<>(16);
        File dir = new File(filePath);
        File[] files = dir.listFiles();
        if (files.length > 0) {
            List<File> fileList = new ArrayList<File>();
            for (File f : files) {
                fileList.add(f);
            }
            Collections.sort(fileList, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    long diff = o1.lastModified() - o2.lastModified();
                    if (diff > 0)
                        return -1;
                    else if (diff == 0)
                        return 0;
                    else
                        return 1;
                }
            });
            deleteOverDateFiles(fileList);
            fileNames = fileList.stream().map(File::getName).collect(Collectors.toList());
        }
        return fileNames;
    }

    private void deleteOverDateFiles(List<File> files) {
        if (files.size() > OVER_DATE_NUM) {
            for (int i = OVER_DATE_NUM; i < files.size(); i++) {
                files.get(i).delete();
            }
            List<File> limitNumList = new ArrayList<>(256);
            for (int i = 0; i < OVER_DATE_NUM; i++) {
                limitNumList.add(files.get(i));
            }
            files = limitNumList;
        }
    }
}
