package cn.tengfei.file;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @Auther: JesseYang
 * @Date: 2018/11/18 10:15
 * @Description:
 */
public interface FileAction {
    //上传文件到指定文件夹 并返回文件名
    String uploadFile(MultipartFile multipartFile, String filePath) throws IOException;

    //获取文件夹下指定数量的文件名，按照名称倒叙排列
    List<String> getFileNamesFromPath(String filePath);

}
