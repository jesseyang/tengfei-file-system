package cn.tengfei.service;

import cn.tengfei.file.FileAction;
import cn.tengfei.ocr.OcrApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class FileOcrServiceImpl implements FileOcrService {

    @Autowired
    FileAction fileAction;

    @Value("${imageFilePath}")
    private String imageFilePath;

    @Autowired
    OcrApi ocrApi;

    @Override
    public List<String> ocrByMultipartFile(MultipartFile file) {
        List<String> result = new ArrayList<>();
        try {
            String fileName = fileAction.uploadFile(file, imageFilePath);
            String filePath = imageFilePath + File.separator + fileName;
            result = ocrApi.ocrByFilePath(filePath);

        } catch (Exception e) {
            e.printStackTrace();
            log.error("upload ocr file error:", e.getMessage());
        }
        return result;
    }
}
