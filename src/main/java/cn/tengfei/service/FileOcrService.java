package cn.tengfei.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileOcrService {
    List<String> ocrByMultipartFile(MultipartFile file);
}
