package cn.tengfei.service;

import cn.tengfei.file.FileAction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Auther: JesseYang
 * @Date: 2018/11/17 23:21
 * @Description:
 */
@Slf4j
@Service
public class FileServiceImpl implements FileService {

    @Autowired
    FileAction fileAction;

    @Value("${imageFilePath}")
    private  String imageFilePath ;

    @Override
    public String uploadFile(MultipartFile multipartFile) {
        String result;
        try {
            result = fileAction.uploadFile(multipartFile, imageFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("upload file error:", e.getMessage());
            result = "上传文件失败，请联系店主";
        }
        return result;
    }

    @Override
    public List<String> getServerFileNameList() {

        return fileAction.getFileNamesFromPath(imageFilePath);
    }


}
