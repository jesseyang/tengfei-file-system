package cn.tengfei.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Auther: JesseYang
 * @Date: 2018/11/17 23:20
 * @Description:
 */
public interface FileService {
    String uploadFile(MultipartFile multipartFile);

    List<String> getServerFileNameList();
}
